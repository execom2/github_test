//
//  EnvironmentViewController.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import UIKit

class EnvironmentViewController: CustomViewController, Storyboarded{
    @IBOutlet weak var environmentChoiseTextField: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet var pickerViewToolbar: UIToolbar!
    var pickerViewDatasource: [String] = []
    
    var preselectedIndex = 0
    
    static var storyboardName: String {
        "Main"
    }
    
    @objc func doneTap(_ sender: Any) {
        print("done")
        
        guard let navController = self.navigationController else {return}
        //check if user is logged
        if UserDefaults.standard.object(forKey: "auth") != nil {
            MainCoordinator.goToPage(page: .home, navigationController: navController, presentationType: .newFlow, showBackButton: false, hideNavbar: false, navbarTitle: "Home")
            //MainCoordinator.goToHome(navigationController: navController, presentationType: .newFlow)
        }
        else {
            //MainCoordinator.goToWelcome(navigationController: navController, presentationType: .newFlow)
            MainCoordinator.goToPage(page: .welcome, navigationController: navController, presentationType: .newFlow, showBackButton: false, hideNavbar: true, navbarTitle: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rightButton = UIBarButtonItem.init(title: "Inizia", style: .done, target: self, action: #selector(doneTap(_:)))
        //rightButton.title = "Inizia"
        
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationItem.rightBarButtonItem?.accessibilityIdentifier = "startBarButton"
        
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        pickerViewDatasource = EnvironmentEnum.allCases.map { $0.rawValue }
        
        if let env = UserDefaults.standard.string(forKey: "env"){
            preselectedIndex = pickerViewDatasource.firstIndex(of: env) ?? 0
        }
        
        environmentChoiseTextField.inputView = pickerView
        environmentChoiseTextField.inputAccessoryView = pickerViewToolbar
        self.pickerView?.selectRow(self.preselectedIndex, inComponent: 0, animated: false)
        environmentChoiseTextField.text = pickerViewDatasource[self.preselectedIndex]
    }
    
    @IBAction func DoneButtonTap(_ sender: Any) {
        environmentChoiseTextField.endEditing(true)
        if let envTxt = environmentChoiseTextField.text, let env = EnvironmentEnum.init(rawValue: envTxt)  {
            UserDefaults.standard.set(env.rawValue, forKey: "env")
            let envFactory = EnvironmentFactory()          
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.selectedEnvironment = envFactory.create(env)
            
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    


}

extension EnvironmentViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewDatasource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewDatasource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        environmentChoiseTextField.text = pickerViewDatasource[row]
        
    }
    
}



