//
//  DevEnvironment.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation

final class DevEnvironment: Environment {
  
    static var sharedInstance = DevEnvironment()

    var type: EnvironmentEnum = .development
    
    var network: NetworkProtocol = DevNetwork()
        
        
    init() {
        NSLog("Dev Environment")
    }
}
