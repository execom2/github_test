//
//  EnvironmentFactory.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation

class EnvironmentFactory {

    func create(_ type: EnvironmentEnum) -> Environment {
        switch type {
        case .development:
            return DevEnvironment.sharedInstance
        case .stub:
            return StubEnvironment.sharedInstance
        case .production:
            return ProdEnvironment.sharedInstance
        }
    }
    
}
