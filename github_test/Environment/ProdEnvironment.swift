//
//  ProdEnvironment.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation

final class ProdEnvironment: Environment {
    static var sharedInstance = ProdEnvironment()

    var type: EnvironmentEnum = .production
    
    var network: NetworkProtocol = ProdNetwork()
    
    
    init() {
        NSLog("Prod Environment")
    }
}
