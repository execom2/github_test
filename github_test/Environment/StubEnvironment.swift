//
//  StubEnvironment.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation

final class StubEnvironment: Environment {
    static var sharedInstance = StubEnvironment()

        
    let type: EnvironmentEnum = .stub
    
    var network: NetworkProtocol = StubNetwork()
    
    
    init() {
        NSLog("Stub Environment")
    }
}
