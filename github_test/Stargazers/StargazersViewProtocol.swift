//
//  HomeViewProtocol.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//

import Foundation

protocol StargazersViewProtocol: AnyObject{
    func operationsCompleted(errorMessage: String?)
}
