//
//  HomeModel.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation

struct StargazersModel {
  
    var owner: String
    var repository: String
    var perPage: Int?
    var page: Int?
    var listStargazers: [GithubResponseDTO]?
}
