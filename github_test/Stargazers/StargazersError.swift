//
//  HomeError.swift
//  github_test
//
//  Created by Danilo De Luca on 10/08/21.
//

import Foundation

enum StargazersError: Error {
    case emptyOwner
    case emptyRepository
    
    var localizedDescription: String {
        switch self {
        case .emptyOwner:
            return "Il campo 'Owner' non può essere vuoto"
        case .emptyRepository:
            return "Il campo 'Repository' non può essere vuoto"
        }
    }

}

