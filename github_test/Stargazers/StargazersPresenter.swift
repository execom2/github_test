//
//  HomePresenter.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation


class StargazersPresenter {

    weak var viewController: StargazersViewProtocol!
    var model: StargazersModel!
    
    init(viewController: StargazersViewProtocol) {
        self.viewController = viewController
        self.model = StargazersModel(owner: "", repository: "")
    }
   
    
    private func validate() -> StargazersError? {
        if self.model.owner.isEmpty {
            return .emptyOwner
        }
        
        if self.model.repository.isEmpty {
            return .emptyRepository
        }
        
        return nil
    }
    
    private func getStarganizersList(isLoadMore: Bool) {
        _ = GithubService()
            .listStargazers(owner: self.model.owner, repository: self.model.repository, perPage: self.model.perPage, page: self.model.page)
            .subscribe(
                onNext: { [weak self] result in
                    
                    if isLoadMore {
                        var page = self?.model.page ?? 1
                        page += 1
                        self?.model.page = page
                        self?.model.listStargazers?.append(contentsOf: result)
                    }
                    else {
                        self?.model.listStargazers = result
                    }
                    self?.viewController.operationsCompleted(errorMessage: nil)
                    return;
                   
                },
                onError: { [weak self] error in
                    // Present error
                    print(error)
                    self?.viewController.operationsCompleted(errorMessage: error.localizedDescription)

                }
         )
    }
    
    func searchButtonTapped(){
        if let error = self.validate() {
            self.viewController.operationsCompleted(errorMessage: error.localizedDescription)
            return
        }
        self.getStarganizersList(isLoadMore: false)
    }
    
    func loadMore() {
        self.getStarganizersList(isLoadMore: true)
    }
    
    
}
