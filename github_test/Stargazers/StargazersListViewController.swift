//
//  StargazersListViewController.swift
//  github_test
//
//  Created by Danilo De Luca on 10/08/21.
//

import UIKit

class StargazersListViewController: CustomViewController, Storyboarded, StargazersViewProtocol {
   
    static var storyboardName: String = "Main"

    var presenter: StargazersPresenter!
    var stargazersModel: StargazersModel?
    var lastItemIndex = 0
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = StargazersPresenter(viewController: self)
        if let model = stargazersModel {
            self.presenter.model = model
            if let list = model.listStargazers {
                lastItemIndex = list.count - 1
            }
        }
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func operationsCompleted(errorMessage: String?) {
        ErrorService.sharedInstance.lastErrorMessage = errorMessage
        LoaderService.sharedInstance.hide()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

        tableView.beginUpdates()
        
        var indexes: [IndexPath] = []
        let firstIndex = lastItemIndex
        
        
        let itemsPerPage = (self.presenter.model.listStargazers?.count ?? 0) - (self.stargazersModel?.listStargazers?.count ?? 0)
        let lastIndex = lastItemIndex + itemsPerPage - 1
        for n in firstIndex...lastIndex {
            indexes.append(IndexPath(row: n, section: 0))
        }
        
        tableView.insertRows(at: indexes, with: .automatic)
        tableView.endUpdates()
        lastItemIndex = self.presenter.model.listStargazers!.count - 1
        self.stargazersModel = self.presenter.model
        
    }
    
  
}

extension StargazersListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.presenter.model.listStargazers?.count ?? 0
        if count == 0 {
            let noDataView = Bundle.main.loadNibNamed("NoDataView", owner: self, options: nil)?.first as! UIView
            noDataView.frame = self.view.frame
            self.view.addSubview(noDataView)
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        let imageView = cell.viewWithTag(1) as! UIImageView
        let indicator = cell.viewWithTag(2) as! UIActivityIndicatorView

        let label = cell.viewWithTag(3) as! UILabel
        let row = self.presenter.model.listStargazers![indexPath.row]
        imageView.tintColor = .systemGray5
        imageView.image = UIImage.init(systemName: "person.circle.fill")

        if let avatarUrl = row.avatarUrl {
            if let url = URL(string: avatarUrl) {
                indicator.startAnimating()
                DispatchQueue.global().async {
                   if let data = try? Data(contentsOf: url) {
                       DispatchQueue.main.async {
                            imageView.image = UIImage(data: data)?.roundedImage
                            indicator.stopAnimating()
                       }
                   }
               }
            }
        }
        
        label.text = row.login
        
        return cell
    }
    
}

extension StargazersListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
  
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            let indicatorFrame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: 80)
            let activityIndicatorView = UIActivityIndicatorView(frame: indicatorFrame)
            activityIndicatorView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
            activityIndicatorView.hidesWhenStopped = true
            self.tableView.tableFooterView = activityIndicatorView
            activityIndicatorView.startAnimating()
            
            self.presenter.loadMore()

        }
    }
 
  

}

