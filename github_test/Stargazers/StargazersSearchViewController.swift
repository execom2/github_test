//
//  HomeViewController.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import UIKit

class StargazersSearchViewController: CustomViewController, StargazersViewProtocol, Storyboarded {
   
    @IBOutlet weak var ownerTxt: UITextField!
    @IBOutlet weak var repositoryTxt: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    static var storyboardName: String = "Main"
    
    var presenter: StargazersPresenter!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = StargazersPresenter(viewController: self)
        self.ownerTxt.delegate = self
        self.repositoryTxt.delegate = self
        /*self.ownerTxt.text = "octocat"
        self.repositoryTxt.text = "hello-world"
        self.txtEditingDidEnd(self.ownerTxt)
        self.txtEditingDidEnd(self.repositoryTxt)*/

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func perPageSegmentedValueChanged(_ sender: UISegmentedControl) {
        self.presenter
            .model.perPage = Int(sender.titleForSegment(at: sender.selectedSegmentIndex) ?? "30")
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        self.scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification, scrollView: UIScrollView) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }

    @IBAction func txtEditingDidEnd(_ sender: UITextField) {
        switch sender {
        case self.ownerTxt:
            self.presenter.model.owner = sender.text ?? ""
        case self.repositoryTxt:
            self.presenter.model.repository = sender.text ?? ""
        default:
            break
        }
    }
    
    @IBAction func searchBtnTap(_ sender: Any) {
        self.view.endEditing(true)
        LoaderService.sharedInstance.show()
        self.presenter.searchButtonTapped()
    }
    
    public func operationsCompleted(errorMessage: String?) {
        ErrorService.sharedInstance.lastErrorMessage = errorMessage
        LoaderService.sharedInstance.hide()
        guard errorMessage == nil else {
            return
        }

        MainCoordinator.goToStargazersList(navigationController: self.navigationController!, presentationType: .push, model: self.presenter.model, showBackButton: true, hideNavbar: false)
        //GOTOSEARCHVIEW
    }
    
    
}

extension StargazersSearchViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true    }
    
}

