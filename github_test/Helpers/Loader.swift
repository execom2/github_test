//
//  Loader.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import UIKit
import RxSwift

class Loader: NSObject {
    static  func  show() -> Observable<Bool> {
        return Observable.create { observer -> Disposable in
            
           
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }

            // topController should now be your topmost view controller
                let view = Bundle.main.loadNibNamed("Loader", owner: self, options: nil)?.first as! UIView
                topController.view.addSubview(view);
            }
            
            
            return Disposables.create()

        }
        
        
    }
    
    
    static  func  show1() {
            
           
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }

            // topController should now be your topmost view controller
                let view = Bundle.main.loadNibNamed("LoaderView", owner: self, options: nil)?.first as! UIView
                view.tag = 987
                topController.view.addSubview(view);
            }
            
        
        
        
    }
}
