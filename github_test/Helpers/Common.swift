//
//  Common.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation

class Common {
    static func readFromConfig(key: String) -> String? {
        var configDictionary: NSDictionary?
        var retValue: String?
        if let path = Bundle.main.path(forResource: "Config", ofType: "plist") {
            configDictionary = NSDictionary(contentsOfFile: path)
            retValue = configDictionary?.value(forKey: key) as? String
        }
        
        return retValue
    }
}
