//
//  JSONEncode.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation

protocol JSONCodable: Codable {
    func toJSONString(outputFormatting: JSONEncoder.OutputFormatting, keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy) -> String?
    
}

extension JSONCodable {
    
   
    
    private func toJSONData(outputFormatting: JSONEncoder.OutputFormatting = JSONEncoder.OutputFormatting.prettyPrinted, keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy = JSONEncoder.KeyEncodingStrategy.useDefaultKeys) -> Data? {
          let encoder = JSONEncoder.init()
          encoder.outputFormatting = outputFormatting
          encoder.keyEncodingStrategy = keyEncodingStrategy

          do {
              let jsonData = try encoder.encode(self)
              return jsonData
          }
          catch {
             return nil
          }
         
      }
    
     func toJSONString(outputFormatting: JSONEncoder.OutputFormatting = JSONEncoder.OutputFormatting.prettyPrinted, keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy = JSONEncoder.KeyEncodingStrategy.useDefaultKeys) -> String? {
           let d = self.toJSONData(outputFormatting: outputFormatting, keyEncodingStrategy: keyEncodingStrategy)
           let str = String(data: d!, encoding: .utf8)!
           return str
       }
    
    
}
    

