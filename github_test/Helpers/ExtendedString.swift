//
//  ExtendedString.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation

extension String {
    func isValidEmail() -> Bool {
       let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
       return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
   }
    
    func hasSpecialCharacters() -> Bool {
        return self.range(of: ".*[^A-Za-z0-9].*", options: .regularExpression) != nil 

    }
    
    func hasCapitalLetters() -> Bool {
        return self.range(of: ".*[A-Z]+.*", options: .regularExpression) != nil

        
        /*let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        return texttest.evaluate(with: self)*/
    }
    
    func hasNumbers() -> Bool {
        return self.range(of: ".*[0-9]+.*", options: .regularExpression) != nil

        /*let numberRegEx  = ".*[0-9]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return texttest.evaluate(with: self)*/
    }
  
 
}
