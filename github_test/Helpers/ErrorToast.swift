//
//  ErrorToast.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import UIKit
import Toast_Swift

class ErrorToast {
    var view: UIView
    var style: ToastStyle
    
    init(view: UIView) {
        self.view = view
        // create a new style
        self.style = ToastStyle()
        // this is just one of many style options
        self.style.messageColor = .white
        self.style.backgroundColor = UIColor.init(red: 0xDC/255, green: 0x35/255, blue: 0x45/255, alpha: 1.0)
    }
    
    public func show(message: String, duration: TimeInterval = 5, position: ToastPosition = .bottom) {
       
        self.view.makeToast(message, duration: duration, position: position, style: self.style)

    }
    
    public func hide() {
        self.view.hideAllToasts()
    }

    
}
