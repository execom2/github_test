//
//  CustomError.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation


protocol CustomErrorProtocol: LocalizedError {
    var code: Int? { get }
    var message: String? { get }
}

struct CustomError: CustomErrorProtocol {
    var code: Int?
    var message: String?
    
    init(code: Int, message: String) {
        self.code = code
        self.message = message
    }
    
}

enum CustomErrorEnum: CustomErrorProtocol {
    
    case InvalidUrl, DecodeDataError, Unauthorized, ShellyBadData, ShellyUnableToConnect, BadNetworkData, GenericError
    
    
    var message: String? {
        return self.getMessage()
    }
    
    var code: Int? {
        return self.getCode()
    }
    
    func getMessage() -> String {
        switch self {
        case .GenericError:
            return "Errore generico"
        case .InvalidUrl:
            return "Url non valido"
        case .BadNetworkData:
            return "Ricevuti dati non validi"
        case .DecodeDataError:
            return "Impossibile decodificare i dati"
        case .Unauthorized:
            return "Non sei autorizzato ad accedere alla risorsa"
        case .ShellyBadData:
            return "Ricevuti dati errati"
        case .ShellyUnableToConnect:
            return "Impossibile eseguire il collegamento a Shelly"
        }
    }
    
    func getCode() -> Int {
        switch self {
        case .GenericError:
            return 500
        case .InvalidUrl:
            return 701
        case .BadNetworkData:
            return 702
        case .DecodeDataError:
            return 601
        case .Unauthorized:
            return 403
        case .ShellyBadData:
            return 602
        case .ShellyUnableToConnect:
            return 603
        }
    }
    

    
}
