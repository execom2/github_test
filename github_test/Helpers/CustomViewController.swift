//
//  ExtendedViewController.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import UIKit

class CustomViewController: UIViewController {
    var hideNavigationBar: Bool?
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let hideNavbar = self.hideNavigationBar {
            self.navigationController?.setNavigationBarHidden(hideNavbar, animated: false)
            self.extendedLayoutIncludesOpaqueBars = hideNavbar;
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)

    }
    

}
