//
//  NetworkProtocol.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import Alamofire

protocol NetworkProtocol {
    var baseUrl: String { get }
    func get(urlPath: UrlEnum, pathParameters: Parameters, queryParameters: Parameters, encoding: ParameterEncoding?, headers: HTTPHeaders?, completion:@escaping (Data?, Error?) -> ())
   
    
}


