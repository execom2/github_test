//
//  Network.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import UIKit
import Alamofire

class DevNetwork: NetworkProtocol {
       
    var baseUrl: String = "https://api.github.com"
    
    
    func createAlamofireRequest(urlPath: UrlEnum, method: HTTPMethod, pathParameters: Parameters, queryParameters: Parameters, encoding: ParameterEncoding?, headers: HTTPHeaders?, completion: @escaping (Data?, Error?) -> ()) {
        var url: String = ""
        if urlPath.rawValue.contains("http://") || urlPath.rawValue.contains("https://") {
            url = urlPath.rawValue
        }
        else {
            url = self.baseUrl + urlPath.rawValue
        }
        
        for item in pathParameters {
            if let key = "{\(item.key)}" as? String, let value = item.value as? String {
                url = url.replacingOccurrences(of: key, with: value)
            }
        }
        
        var  pathParamsLoopStarted = false
        for item in queryParameters {
            if pathParamsLoopStarted == false {
                url.append("?\(item.key)=\(item.value)")
                pathParamsLoopStarted = true
            }
            else {
                url.append("&\(item.key)=\(item.value)")

            }
        }
    
        
        let encoding = encoding ?? JSONEncoding.default

        AF.request(url, method: method, parameters: nil, encoding: encoding, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success:
                guard let data = response.data else {
                    completion(nil, response.error ?? CustomErrorEnum.BadNetworkData)
                    return;
                }
                completion(data, nil)
            case .failure(let error):
                completion(nil, error);
            }
        }
    }
    
    func get(urlPath: UrlEnum, pathParameters: Parameters, queryParameters: Parameters, encoding: ParameterEncoding?, headers: HTTPHeaders?, completion: @escaping (Data?, Error?) -> ()) {
        self.createAlamofireRequest(urlPath: urlPath, method: .get, pathParameters: pathParameters, queryParameters: queryParameters, encoding: encoding, headers: headers) { response, error in
            completion(response, error)
        }
    }
    
   
    
   
    
   
    
}
