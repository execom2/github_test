//
//  Stub.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import UIKit
import Alamofire

class StubNetwork: NetworkProtocol {
    
    var baseUrl: String = ""
    
    private func getFileFromUrlPath(url: UrlEnum) -> String? {
       
        switch url {
        case .LIST_STARGAZERS:
            return "stargazers"
        }
        
    }
    
    
    func get(urlPath: UrlEnum, pathParameters: Parameters, queryParameters: Parameters, encoding: ParameterEncoding?, headers: HTTPHeaders?, completion: @escaping (Data?, Error?) -> ()) {
        
        let filename = self.getFileFromUrlPath(url: urlPath)
        guard let path = Bundle.main.path(forResource: filename, ofType: "json") else {
            completion(nil, CustomError(code: -1, message: "Impossibile trovare il file indicato per i dati stub"))
            return
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let jsonresult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves )
            completion(data, nil)
        
          } catch  {
            completion(nil, error)
          }
    }
    
    
    

}
