//
//  WelcomePagePresenter.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
class WelcomePagePresenter {
    
    weak var welcomePageViewController: WelcomePageViewProtocol!
    var model: WelcomePageModel

    init(viewController: WelcomePageViewProtocol) {
        self.welcomePageViewController = viewController
        self.model = WelcomePageModel()
    }
    
    func getSlides(){
        
        self.model.slides.append(Slide.init(title: "Benvenuto in Github_Test", text: "Benvenuto nell'app Github_Test. Quest'app è stata pensata per poter mettere alla prova dei candidati che verranno scelti per entrare a far parte dei team di Subito.it"))
        self.model.slides.append(Slide.init(title: "Mettiti alla prova", text: "Il candidato deve dare prova della propria capacità di far fronte alle varie problematiche presenti all'interno di una app scritta in Swift"))
        self.welcomePageViewController.operationsCompleted(errorMessage: nil)

    }
    
    
}
