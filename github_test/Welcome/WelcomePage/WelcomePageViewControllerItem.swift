//
//  WelcomePageViewControllerItemViewController.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import UIKit

class WelcomePageViewControllerItem: UIViewController {
    
    var pageTitle: String?
    var pageDescription: String?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    
    
    internal static func instantiate(with pageTitle: String, pageDescription: String) -> WelcomePageViewControllerItem {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let vc = storyboard.instantiateViewController(identifier: "WelcomePageViewControllerItemViewController") as WelcomePageViewControllerItem
        vc.pageTitle = pageTitle
        vc.pageDescription = pageDescription
        return vc
    }


   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let titleLabel = self.titleLabel {
            titleLabel.text=self.pageTitle
        }
        if let descriptionTextView = self.descriptionTextView {
            descriptionTextView.text = self.pageDescription
            descriptionTextView.sizeToFit()
        }
        // Do any additional setup after loading the view.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
