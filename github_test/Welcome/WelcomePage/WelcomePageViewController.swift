//
//  WelcomePageViewController.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import UIKit

class WelcomePageViewController: UIPageViewController, WelcomePageViewProtocol {

    var presenter: WelcomePagePresenter!
    fileprivate var items: [UIViewController] = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = WelcomePagePresenter(viewController: self)
        LoaderService.sharedInstance.show()
        self.presenter.getSlides()
    }
    
    func operationsCompleted(errorMessage: String?) {
        if errorMessage == nil {
            for slide in self.presenter.model.slides {
                let item = WelcomePageViewControllerItem.instantiate(with: slide.title ?? "", pageDescription: slide.text ?? "")
                self.items.append(item)
            }
            
            self.dataSource = self
            self.delegate = self
            if let firstViewController = self.items.first {
                self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
            }
        }
        
        ErrorService.sharedInstance.lastErrorMessage = errorMessage
        LoaderService.sharedInstance.hide()
    }
    
    
    fileprivate func populateItems() {
        /*LoaderService.sharedInstance.show()
        WelcomeService()
            .getWelcomeSlides()
            .subscribe(
                onNext: { [weak self] slides in
                    LoaderService.sharedInstance.hide()

                    for slide in slides {
                        let item = WelcomePageViewControllerItem.instantiate(with: slide.title ?? "", pageDescription: slide.text ?? "")
                        self?.items.append(item)
                    }
                    
                    self?.dataSource = self
                    self?.delegate = self
                    if let firstViewController = self?.items.first {
                        self?.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
                    }
                },
                onError: { [weak self] error in
                    // Present error
                    LoaderService.sharedInstance.hide()
                    print(error)
                    let errorToast = ErrorToast.init(view: (self?.view)!)
                    errorToast.show(message: error.localizedDescription)
                }
         )*/
        
        /*let item1 = WelcomePageViewControllerItem.instantiate(with: "Benvenuto su Fonorica", pageDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
        let item2 = WelcomePageViewControllerItem.instantiate(with: "Benvenuto su Fonorica 1", pageDescription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.")
        let item3 = WelcomePageViewControllerItem.instantiate(with: "Benvenuto su Fonorica 2", pageDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")

        self.items.append(item1)
        self.items.append(item2)
        self.items.append(item3)*/

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WelcomePageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = items.firstIndex(of: viewController)!

        // This version will not allow pages to wrap around
        guard currentIndex > 0 else {
            return nil
        }

        return items[currentIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = items.firstIndex(of: viewController)!

        // This version will not allow pages to wrap around
        guard currentIndex < items.count - 1 else {
            return nil
        }

        return items[currentIndex + 1]
        
        
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        setupPageControl()
        return self.items.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = .gray
        appearance.currentPageIndicatorTintColor = .white
        appearance.backgroundColor = .clear
    }

}


extension WelcomePageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
    }
}
