//
//  WelcomePageViewProtocol.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
protocol WelcomePageViewProtocol: AnyObject {
    func operationsCompleted(errorMessage: String?)

}
