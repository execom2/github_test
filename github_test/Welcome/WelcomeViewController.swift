//
//  AuthViewController.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import RxSwift
import WebKit
import ZSWTappableLabel

class WelcomeViewController: CustomViewController, WelcomeViewProtocol, Storyboarded {
        
    static var storyboardName: String = "Main"
    var frameworkViewController: UIViewController?
    
    @IBOutlet weak var termsLbl: ZSWTappableLabel!
    
    @IBOutlet weak var accessLbl: ZSWTappableLabel!
    
    @IBOutlet var facebookButton: UIButton!
    
    @IBOutlet var googleButton: UIButton!
    
    @IBOutlet var appleButton: UIButton!
    
    var presenter: WelcomePresenter!

    func operationsCompleted(errorMessage: String?, type: WelcomeOperationType) {
        ErrorService.sharedInstance.lastErrorMessage = errorMessage
        LoaderService.sharedInstance.hide()
        if errorMessage == nil {
            switch type {
            case .GO_TO_HOME:
                print("login with apple")
            case .SHOW_INFOBOX:
                print("Login with fb")
            }
        }
    }
    
    enum UrlType: String {
        case terms = "https://www.fonorica.it/terminiecondizioni"
    }
    
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.barStyle = .default

        let detector = try! NSDataDetector(types: NSTextCheckingAllTypes)

        let string = "cliccando sulle icone accetti \(UrlType.terms.rawValue) di Github_Test"
        let attributedString = NSMutableAttributedString(string: string, attributes: nil)
        let range = NSRange(location: 0, length: (string as NSString).length)
        
        var attributes = [NSAttributedString.Key: Any]()
        attributes[.tappableRegion] = true
        attributes[.tappableHighlightedBackgroundColor] = UIColor.clear
        attributes[.tappableHighlightedForegroundColor] = UIColor.white

        attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue

        detector.enumerateMatches(in: attributedString.string, options: [], range: range) { (result, flags, _) in
          guard let result = result else { return }
            
          attributes[.init(rawValue: "NSTextCheckingResult")] = result

          attributedString.addAttributes(attributes, range: result.range)
        }
        
        let replacementsTerms = [("termini e condizioni", URL(string: UrlType.terms.rawValue)!)]
        
        replacementsTerms.enumerated().forEach { index, value in
            attributes[.hyperlink] = value.1.absoluteString
            attributes[.foregroundColor] = UIColor.white

           let urlAttributedString = NSAttributedString(string: value.0, attributes: attributes)
           let range = (attributedString.string as NSString).range(of: value.1.absoluteString)
            attributedString.replaceCharacters(in: range, with: urlAttributedString)
            
        }
        
        termsLbl.attributedText = attributedString
        termsLbl.tapDelegate = self
        
        
       
        self.presenter = WelcomePresenter(viewController: self)
       
    }
    
    
  
    
    @IBAction func startBtnTap(_ sender: Any) {
        MainCoordinator.goToPage(page: .home, navigationController: self.navigationController!, presentationType: .push, showBackButton: true, hideNavbar: false, navbarTitle: "Home")
    }
}

extension WelcomeViewController: ZSWTappableLabelTapDelegate {
   
    func tappableLabel(
      _ tappableLabel: ZSWTappableLabel,
        tappedAt idx: Int,
      withAttributes attributes: [NSAttributedString.Key : Any]
    ) {
        guard let hyperlinkTypeAttribute = attributes[.hyperlink] as? String, let hyperlinkType = UrlType.init(rawValue: (hyperlinkTypeAttribute))  else {
            return;
        }
        
        switch hyperlinkType {
            case UrlType.terms:
                let title = "Termini e condizioni"
                let message = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Chiudi", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
     
    }

}


extension NSAttributedString.Key {
    static let hyperlink = NSAttributedString.Key("hyperlink")
}

