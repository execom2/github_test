//
//  AuthPresenter.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation



class WelcomePresenter {
    
    weak var welcomeViewController: WelcomeViewProtocol!
    var model: WelcomeModel
    
    init(viewController: WelcomeViewProtocol) {
        self.welcomeViewController = viewController
        self.model = WelcomeModel()

    }
    
    
    
}


