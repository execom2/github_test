//
//  mainCoordinator.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator {
    
    enum PresentationType {
        case modal
        case newFlow
        case push
    }
    
    enum PageType {
        case environment
        case welcome
        case home
    }
    
    
    private static func presentVC(navigationController: UINavigationController, viewController: UIViewController, presentationType: PresentationType) {
        
        //navigationController.navigationBar.isTranslucent = false
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor(red: 9/255, green: 18/255, blue: 40/255, alpha: 1)
        //navigationController.navigationBar.barTintColor = UIColor(red: 9/255, green: 18/255, blue: 40/255, alpha: 1)

        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 18)!
        ]

        UINavigationBar.appearance().titleTextAttributes = attrs
        
        switch presentationType {
               case .modal:
                viewController.modalPresentationStyle = .fullScreen
                   navigationController.present(viewController, animated: true, completion: nil)
               case .newFlow:
                   navigationController.viewControllers.removeAll()
                   navigationController.viewControllers.append(viewController)
               default:
                   navigationController.pushViewController(viewController, animated: true)
               }
    }
    
    static func goToHome(navigationController: UINavigationController, presentationType: PresentationType) {
        let vc = StargazersSearchViewController.instantiate()
        vc.navigationItem.title = "Github_Test"
        MainCoordinator.presentVC(navigationController: navigationController, viewController: vc, presentationType: presentationType)
       
    }
       
    static func goToPage(page: PageType,navigationController: UINavigationController, presentationType: PresentationType, showBackButton: Bool, hideNavbar: Bool, navbarTitle: String?) {
        var vc: UIViewController!
        
        switch page {
        case .environment:
            vc = EnvironmentViewController.instantiate()
        case .welcome:
            vc = WelcomeViewController.instantiate()
        case .home:
            vc = StargazersSearchViewController.instantiate()
        }
        

        if showBackButton {
            let backBtn = UIBarButtonItem(image: UIImage(named: "back"),
                                          style: .plain,
                                          target: navigationController,
                                          action: #selector(UINavigationController.popViewController(animated:)))
            vc.navigationItem.leftBarButtonItem = backBtn
        }
        else {
            vc.navigationItem.hidesBackButton = true
        }
        
        navigationController.setNavigationBarHidden(hideNavbar, animated: false)
        
        if let vcCustom = vc as? CustomViewController {
            vcCustom.hideNavigationBar = hideNavbar
        }

        vc.navigationItem.title = navbarTitle
        MainCoordinator.presentVC(navigationController: navigationController, viewController: vc, presentationType: presentationType)

    }
    
    static func goToStargazersList(navigationController: UINavigationController, presentationType: PresentationType, model: StargazersModel?, showBackButton: Bool, hideNavbar: Bool) {
        let vc = StargazersListViewController.instantiate()
        vc.stargazersModel = model
        vc.navigationItem.title = "Stargazers"
        
        if showBackButton {
            let backBtn = UIBarButtonItem(image: UIImage(named: "back"),
                                          style: .plain,
                                          target: navigationController,
                                          action: #selector(UINavigationController.popViewController(animated:)))
            vc.navigationItem.leftBarButtonItem = backBtn
        }
        else {
            vc.navigationItem.hidesBackButton = true
        }
        
        navigationController.setNavigationBarHidden(hideNavbar, animated: false)
        
        vc.hideNavigationBar = hideNavbar

        MainCoordinator.presentVC(navigationController: navigationController, viewController: vc, presentationType: presentationType)
       
    }
    
    
}



