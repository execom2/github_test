//
//  StoryboardedProtocol.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import UIKit

protocol Storyboarded {
    static var storyboardName: String {get}
    static func instantiate()->Self
    
}

extension Storyboarded where Self: UIViewController {
    
    
    static func instantiate()->Self {
        let vcIdentifier = NSStringFromClass(self).components(separatedBy: ".")[1]
        let storyboard = UIStoryboard.init(name: self.storyboardName, bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: vcIdentifier)
        return vc as! Self
       
    }
    
}

extension Storyboarded where Self: UITabBarController {
    
    
    static func instantiate()->Self {
        let vcIdentifier = NSStringFromClass(self).components(separatedBy: ".")[1]
        let storyboard = UIStoryboard.init(name: self.storyboardName, bundle: nil)
        
        let vc = storyboard.instantiateViewController(identifier: vcIdentifier)
        return vc as! Self
       
    }
    
}
