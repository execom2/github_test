//
//  ErrorService.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

struct ToastOptions {
    var duration: TimeInterval
    var position: ToastPosition
    var style: ToastStyle?
}

final class ErrorService: Singleton {
    static var sharedInstance = ErrorService()
    public var lastErrorMessage: String?
    

    
    public func show(view: UIView, options: ToastOptions?) {
        guard let errorMessage = self.lastErrorMessage else {
            return
        }
        let errorToast = ErrorToast(view: view)
        errorToast.view.accessibilityIdentifier = "errorToastView"
        if let toastOptions = options {
            if let style = options?.style {
                errorToast.style = style
            }
            errorToast.show(message: errorMessage, duration: toastOptions.duration, position: toastOptions.position)
        }
        else {
            errorToast.show(message: errorMessage)
        }
        self.lastErrorMessage = nil
    }
    

}
