//
//  LoginService.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class GithubService {
    private var network: NetworkProtocol?
    init() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        NSLog("environment network host %@", appDelegate.selectedEnvironment?.type.rawValue ?? "")
        NSLog("environment network  baseUrl %@", appDelegate.selectedEnvironment?.network.baseUrl  ?? "")
        self.network = appDelegate.selectedEnvironment?.network
      
    }
    
    func  listStargazers(owner: String, repository: String, perPage: Int?, page: Int?) -> Observable<[GithubResponseDTO]> {
        return Observable.create { observer -> Disposable in

            if let network = self.network {
                let httpHeaders = HTTPHeaders([:])

                let pathParameters = ["owner" : owner, "repo": repository]
                let queryParamenters = ["per_page": String(perPage ?? 30), "page": String(page ?? 1)]
                network.get(urlPath: .LIST_STARGAZERS, pathParameters: pathParameters, queryParameters: queryParamenters, encoding: URLEncoding.default, headers: httpHeaders , completion: { result, error in
                    guard let data = result else {
                        observer.onError(error ?? CustomErrorEnum.GenericError)
                        return;
                    }
                 
                    do {
    
                        let jsonResult = try JSONSerialization.jsonObject(with: data, options: [])
                        let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: .prettyPrinted)
                        let printableJSON = String(data: jsonData, encoding: .utf8)
                        print(printableJSON ?? "")
                        
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        decoder.dateDecodingStrategy = .secondsSince1970
                        let githubResponse = try decoder.decode([GithubResponseDTO].self, from: data)
                        observer.onNext(githubResponse)
                       
                    } catch  {
                        observer.onError(error)
                    }
                
                    
                })
            }
            
            return Disposables.create()

        }
        
        
    }
    
 
    

}
