//
//  LoaderService.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//  Copyright © 2021 Danilo De Luca. All rights reserved.
//

import Foundation
import RxSwift

final class LoaderService: Singleton {
    static var sharedInstance = LoaderService()
    private var lastShowTime: Date?
    public var isLoading: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    
    public func show() {
        self.lastShowTime = Date()
        self.isLoading.onNext(true)
    }
    
    public func hide() {
        if let last = self.lastShowTime {
            var interval = Date().timeIntervalSince(last)
         
                
            if let threshold =  Double(Common.readFromConfig(key: "networkLoaderThreshold") ?? "0") {
                /*while interval < threshold {
                    interval = Date().timeIntervalSince(last)
                    print("threshold:\(threshold); interval:\(interval)")
                }
                self.isLoading.onNext(false)*/
                let delay = threshold - interval
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) { // Change `2.0` to the desired number of seconds.
                   // Code you want to be delayed
                    self.isLoading.onNext(false)

                }

            }
            
        }
    }
}
