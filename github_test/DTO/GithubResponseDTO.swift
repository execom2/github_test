//
//  GithubResponseDTO.swift
//  github_test
//
//  Created by Danilo De Luca on 10/08/21.
//

import Foundation

struct GithubResponseDTO: JSONCodable {
    var login: String?
    var id: Int
    var nodeId: String?
    var avatarUrl: String?
    var gravatarId: String?
    var url: String?
    var htmlUrl: String?
    var followersUrl: String?
    var followingUrl: String?
    var gistsUrl: String?
    var starredUrl: String?
    var subscriptionsUrl: String?
    var organizationsUrl: String?
    var reposUrl: String?
    var eventsUrl: String?
    var receivedEventsUrl: String?
    var type: String?
    var siteAdmin: Bool
    
    private enum CodingKeys: String, CodingKey {
        case login
        case id
        case nodeId
        case avatarUrl
        case gravatarId
        case url
        case htmlUrl
        case followersUrl
        case followingUrl
        case gistsUrl
        case starredUrl
        case subscriptionsUrl
        case organizationsUrl
        case reposUrl
        case eventsUrl
        case receivedEventsUrl
        case type
        case siteAdmin
    }
  
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.login = (try? container.decode(String.self, forKey: .login))
        self.id = (try? container.decode(Int.self, forKey: .id)) ?? -1
        self.nodeId = (try? container.decode(String.self, forKey: .nodeId))
        self.avatarUrl = (try? container.decode(String.self, forKey: .avatarUrl))
        self.gravatarId = (try? container.decode(String.self, forKey: .gravatarId))
        self.url = (try? container.decode(String.self, forKey: .url))
        self.htmlUrl = (try? container.decode(String.self, forKey: .htmlUrl))
        self.followersUrl = (try? container.decode(String.self, forKey: .followersUrl))
        self.followingUrl = (try? container.decode(String.self, forKey: .followingUrl))
        self.gistsUrl = (try? container.decode(String.self, forKey: .gistsUrl))
        self.starredUrl = (try? container.decode(String.self, forKey: .starredUrl))
        self.subscriptionsUrl = (try? container.decode(String.self, forKey: .subscriptionsUrl))
        self.organizationsUrl = (try? container.decode(String.self, forKey: .organizationsUrl))
        self.reposUrl = (try? container.decode(String.self, forKey: .reposUrl))
        self.eventsUrl = (try? container.decode(String.self, forKey: .eventsUrl))
        self.receivedEventsUrl = (try? container.decode(String.self, forKey: .receivedEventsUrl))
        self.type = (try? container.decode(String.self, forKey: .type))
        self.siteAdmin = (try? container.decode(Bool.self, forKey: .siteAdmin)) ?? false
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(self.login, forKey: .login)
        try? container.encode(self.id, forKey: .id)
        try? container.encode(self.nodeId, forKey: .nodeId)
        try? container.encode(self.avatarUrl, forKey: .avatarUrl)
        try? container.encode(self.gravatarId, forKey: .gravatarId)
        try? container.encode(self.url, forKey: .url)
        try? container.encode(self.htmlUrl, forKey: .htmlUrl)
        try? container.encode(self.followersUrl, forKey: .followersUrl)
        try? container.encode(self.followingUrl, forKey: .followingUrl)
        try? container.encode(self.gistsUrl, forKey: .gistsUrl)
        try? container.encode(self.starredUrl, forKey: .starredUrl)
        try? container.encode(self.subscriptionsUrl, forKey: .subscriptionsUrl)
        try? container.encode(self.organizationsUrl, forKey: .organizationsUrl)
        try? container.encode(self.reposUrl, forKey: .reposUrl)
        try? container.encode(self.eventsUrl, forKey: .eventsUrl)
        try? container.encode(self.receivedEventsUrl, forKey: .receivedEventsUrl)
        try? container.encode(self.type, forKey: .type)
        try? container.encode(self.siteAdmin, forKey: .siteAdmin)
    }
    
    
    
    
}
