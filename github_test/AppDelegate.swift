//
//  AppDelegate.swift
//  github_test
//
//  Created by Danilo De Luca on 09/08/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var selectedEnvironment: Environment? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
     
        // Modifico l'appearance della barra di navigazione
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "pageBackground")
        appearance.titleTextAttributes = [
            .foregroundColor: UIColor(named: "textWhite"),
            .font: UIFont(name: "Poppins-regular", size: 18) as Any
        ]
        UIBarButtonItem.appearance().tintColor = UIColor(named: "textWhite")
        
        LoaderService.sharedInstance
            .isLoading
            .subscribe(
                onNext: { [weak self] loading in
                    // Store value
                   
                        
                     let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                     if var topController = keyWindow?.rootViewController {
                        /*if topController.presentedViewController == nil{
                            if topController is UINavigationController {
                                if let nav = topController as? UINavigationController {
                                    topController = nav.viewControllers.last!
                                }
                            }
                            
                        }*/
                         while let presentedViewController = topController.presentedViewController {
                             topController = presentedViewController
                         }
                        
                        if loading {
                            let view = Bundle.main.loadNibNamed("LoaderView", owner: self, options: nil)?.first as! UIView
                            view.frame = topController.view.frame
                            view.tag = 987
                            topController.view.addSubview(view);
                        }
                        else {
                            if let view = topController.view.viewWithTag(987) {
                                view.removeFromSuperview()
                            }
                            ErrorService.sharedInstance.show(view: topController.view, options: nil)
                            
                        }
                        
                     }
                   
                },
                onError: { [weak self] error in
                    // Present error
                    print(error)
                }
            )

      
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

