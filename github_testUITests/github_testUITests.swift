//
//  github_testUITests.swift
//  github_testUITests
//
//  Created by Danilo De Luca on 09/08/21.
//

import XCTest

class github_testUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

   
    /*func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }*/
    
    
    func testStargazersSearchWithOwnerEmpty() throws {
        // con questo test verifichiamo che non scrivendo alcun testo nel campo "ownerTxt" la app restituisce un errore
        let app = XCUIApplication()
        app.launch()
        app.navigationBars.buttons["startBarButton"].tap()
        
        // si apre la schermata di benvenuto e si tappa sul pulsante start
        app.buttons["startBtn"].tap()

        let repositoryTxt = app.textFields["repositoryTxt"]
        repositoryTxt.tap()
        // con il metodo successivo si scrive un testo nel textfield
        repositoryTxt.typeText("hello-worl")
        // con il metodo successivo si scrive un singolo carattere
        app.keys["q"].tap()
        app.keyboards.buttons["Done"].tap()

        app.buttons["searchBtn"].tap()
        
        let toastView = app.otherElements["errorToastView"]
        XCTAssertTrue(toastView.waitForExistence(timeout: 3))

        //XCTAssert(app.otherElements["errorToastView"].exists)
        
    }
    
    func testStargazersSearchWithRepositoryEmpty() throws {
        // con questo test verifichiamo che non scrivendo alcun testo nel campo "ownerTxt" la app restituisce un errore
        let app = XCUIApplication()
        app.launch()
        app.navigationBars.buttons["startBarButton"].tap()
        
        // si apre la schermata di benvenuto e si tappa sul pulsante start
        app.buttons["startBtn"].tap()

        let ownerTxt = app.textFields["ownerTxt"]
        ownerTxt.tap()
        // con il metodo successivo si scrive un testo nel textfield
        ownerTxt.typeText("octocat")
        app.buttons["searchBtn"].tap()
        
        XCTAssertTrue(app.otherElements["errorToastView"].exists)
    }
    
    func testStargazersSearchWithOwnerAndRepositoryNotEmpty() throws {
        // con questo test verifichiamo che non scrivendo alcun testo nel campo "ownerTxt" la app restituisce un errore
        let app = XCUIApplication()
        app.launch()
        app.navigationBars.buttons["startBarButton"].tap()
        
        // si apre la schermata di benvenuto e si tappa sul pulsante start
        app.buttons["startBtn"].tap()

        let ownerTxt = app.textFields["ownerTxt"]
        ownerTxt.tap()
        ownerTxt.typeText("octocat")
        
        let repositoryTxt = app.textFields["repositoryTxt"]
        repositoryTxt.tap()
        repositoryTxt.typeText("Testo")
        
        app.buttons["searchBtn"].tap()
        
        XCTAssertFalse(app.otherElements["errorToastView"].exists)
    }
}
